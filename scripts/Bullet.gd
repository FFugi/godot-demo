extends KinematicBody2D

export var motion = Vector2()
export var hitPoints = 20
var isAlive = true
var source = self

func _ready():
	$LifeTime.start()
	pass

func _physics_process(delta):
	if $DeadTimer.get_time_left() == 0 and not isAlive:
		get_parent().remove_child(self)
		return
	if $LifeTime.get_time_left() == 0:
		destroy()
		return
	var collision = move_and_collide(motion * delta)
	if collision:
		var src = null
		if collision.collider.has_method("get_source"):
			src = collision.collider.get_source()
		hit(0, src)
		if collision.collider.has_method("hit"):
			collision.collider.hit(hitPoints, source)
		destroy()

func destroy():
	if $DeadTimer.is_stopped():
		$CollisionShape2D.disabled = true
		$Sprite.visible = false
		$DeadTimer.start()
		motion = Vector2()
		isAlive = false
	
func hit(points, source):
	$Particles2D.emitting = true
	destroy()
	
func get_source():
	return source
	
func get_hit_points():
	return hitPoints
	
func set_source(obj):
	source = obj