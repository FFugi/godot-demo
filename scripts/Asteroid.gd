extends KinematicBody2D

var motion = Vector2(-50, 0)
export var hitPoints = 10000
var rotateSpeed = null
var rotateDir = -1

func _ready():
	rotateSpeed = rand_range(0.6, 2)
	draw_rotate_dir()
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _physics_process(delta):
	rotate(rotateDir * rotateSpeed * delta)
	var collision = move_and_collide(delta * motion)
	if collision:
		if collision.collider.has_method("hit"):
			collision.collider.hit(hitPoints, self)
		
func draw_rotate_dir():
	if randi() % 2 == 1:
		rotateDir = -1
	else:
		rotateDir = 1

func get_hit_points():
	return hitPoints