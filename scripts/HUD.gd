extends CanvasLayer

var score = 0

func _ready():
	
	pass
	
func player_died():
	$DiedLabel.visible = true
	
func respawn():
	$DiedLabel.visible = false
	increase_score(-score)
	pass
	
func increase_score(value):
	score += value
	$ScoreLabel.text = "SCORE: " + String(score)
	
func change_health_bar(object):
	if object.has_method("get_health_ratio"):
		$HealthBar.value = object.get_health_ratio() * 100

