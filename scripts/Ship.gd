extends KinematicBody2D

export var health = 100
export var reloadTime = 0.5
export var bulletStartOffset = 27
export var aimDistortion = 0.04
export var agility = 400

var hitPoints = 1000
var motion = Vector2(0, 0)
const MAX_VERTICAL_SPEED = 300
var BULL_SPEED = 400
const ROTATION = 20
var isLeftGunEnabled = false
var isAlive = true
var baseRotation = null
var maxHealth = 100
var	bulletScene = preload("res://scenes/source/Bullet.tscn")

func _ready():	
	baseRotation = rotation_degrees
	$ReloadTimer.set_wait_time(reloadTime)
	maxHealth = health
	pass
	
func set_ship_color(color):
	$Sprite.set_modulate(color)
	
func destroy():
	isAlive = false
	$Sprite.visible = false
	$Particles2D.emitting = false
	$ExplosionParticles.emitting = true
	$CollisionPolygon2D.disabled = true
	
func make_alive():
	health = maxHealth
	isAlive = true
	$Sprite.visible = true
	$Particles2D.emitting = true
	$ExplosionParticles.emitting = false
	$CollisionPolygon2D.disabled = false
	
func update_alive_status():
	if health <= 0:
		destroy()

func hit(points, source):
	health -= points
	update_alive_status()

func get_hit_points():
	return hitPoints

func go_up(delta, isFlipped):
	motion.y = max(motion.y - agility * delta, -MAX_VERTICAL_SPEED)
	if isFlipped:
		rotation_degrees = baseRotation - ROTATION * (motion.y/MAX_VERTICAL_SPEED)
	else:
		rotation_degrees = baseRotation + ROTATION * (motion.y/MAX_VERTICAL_SPEED)
	
func go_down(delta, isFlipped):	
	motion.y = min(motion.y + agility * delta, MAX_VERTICAL_SPEED)
	if isFlipped:
		rotation_degrees = baseRotation - ROTATION * (motion.y/MAX_VERTICAL_SPEED)
	else:
		rotation_degrees = baseRotation + ROTATION * (motion.y/MAX_VERTICAL_SPEED)
	
func go_nowhere(delta, isFlipped):
	if isFlipped:
		rotation_degrees = baseRotation - ROTATION * (motion.y/MAX_VERTICAL_SPEED)
	else:
		rotation_degrees = baseRotation + ROTATION * (motion.y/MAX_VERTICAL_SPEED)
	if motion.y > 2:
		motion.y -= 200 * delta
	elif motion.y < -2:
		motion.y += 200 * delta
	else:
		motion.y = 0
	pass	
	
func shoot():
	if not $ReloadTimer.is_stopped() or not isAlive:
		return

	$ReloadTimer.start()
	var instancedNode = bulletScene.instance()
	var startPosition = Vector2(bulletStartOffset, 0)
	
	var bulletRotation = rotation
	var distortion = rand_range(aimDistortion/2, aimDistortion)
	if isLeftGunEnabled:
		startPosition.y -= 5
		bulletRotation -= distortion
	else:
		startPosition.y += 5
		bulletRotation += distortion

	startPosition = startPosition.rotated(bulletRotation)
	isLeftGunEnabled = not isLeftGunEnabled
	
	instancedNode.position = startPosition + position
	instancedNode.rotation = rotation
	instancedNode.set_source(self)
	var bullMotion = Vector2(1, 0)

	instancedNode.motion =  bullMotion.rotated(bulletRotation) * BULL_SPEED
	get_parent().add_child(instancedNode)
	
func get_health_ratio():
	return float(health) / maxHealth

func increase_health(value):
	health = min(health + value, maxHealth)