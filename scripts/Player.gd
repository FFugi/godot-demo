extends "res://scripts/Ship.gd"


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _physics_process(delta):
	
	var rot = get_transform().get_rotation()
	if Input.is_action_pressed("ui_up"):
		motion.y = -VER_SPEED
		rotation_degrees = -ROTATION
	elif Input.is_action_pressed("ui_down"):
		motion.y = VER_SPEED
		rotation_degrees = ROTATION
	else:
		rotation_degrees = 0
		motion.y = 0
	
	if Input.is_action_pressed("ui_accept") and $ReloadTimer.get_time_left() == 0:
		shoot()
	
	var collision = move_and_collide(motion * delta)

	if collision:
		if collision.collider.has_method("get_hit_points"):
			hit(collision.collider.get_hit_points())
		if collision.collider.has_method("hit"):
			collision.collider.hit(hitPoints)

func make_alive():
	isAlive = true
	isWaiting = false
	$Sprite.visible = true
	$Particles2D.emitting = true
	$ExplosionParticles.emitting = false
	$CollisionPolygon2D.disabled = false
