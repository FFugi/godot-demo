extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var motion = Vector2(-50, 0)
var isAlive  = true

func _ready():
	pass

func _physics_process(delta):
	if isAlive:
		var result = move_and_collide(motion * delta)
		if result:
			isAlive = false
			$Sprite.visible = false
			$CollisionShape2D.disabled = true
			$PickParticles.emitting = true
			$DeadTimer.start()
			if result.collider.has_method("increase_health"):
				result.collider.increase_health(100)
	else:
		if $DeadTimer.get_time_left() == 0:
			get_parent().remove_child(self)
