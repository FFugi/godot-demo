extends Node

var isReady = false
var startX = null
const shipScene = null
const asteroidScene = null
const healthPickupScene = null

func _ready():
	$Timer.start()
	startX = get_viewport().get_visible_rect().size.x + 200
	shipScene = load("res://scenes/source/EnemyShip.tscn")
	asteroidScene = load("res://scenes/source/Asteroid.tscn")
	healthPickupScene = load("res://scenes/source/HealthPickup.tscn")
	pass

func _process(delta):
	randomize()
	if $Timer.time_left == 0:
		var visible_rect = get_viewport().get_visible_rect()
		var yPos = rand_range(64, visible_rect.size.y - 64)
		var position = Vector2(visible_rect.end.x + 200, visible_rect.end.y - yPos)
		var type = randi() % 100
		if type < 50:
			spawnShipColumn(randi() % 6 + 3, yPos)
		elif type < 95:
			spawnAsteroid(Vector2(startX, yPos))
			$Timer.set_wait_time(0.5)
		else:
			spawnHealthPickup(Vector2(startX, yPos))
			$Timer.set_wait_time(0.1)
		$Timer.start()

func spawnShipColumn(shipCount, yPos):
	for i in range(0, shipCount):
		spawnShip(Vector2(startX + 96 * i, yPos))
	$Timer.set_wait_time(shipCount/0.9)
	pass
	
func spawnShipKey():
	
	pass
	
func spawnShip(position):
	var node = shipScene.instance()
	node.position = position
	$Container.add_child(node)
	
func spawnAsteroid(position):
	var node = asteroidScene.instance()
	node.position = position
	$Container.add_child(node)
	
func spawnHealthPickup(position):
	var node = healthPickupScene.instance()
	node.position = position
	$Container.add_child(node)