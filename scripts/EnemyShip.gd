extends "res://scripts/Ship.gd"

var rayOffset = 17
export var raycastCollisionMask = 4
signal got_hit
signal got_destroyed

func _ready():
	var CanvasNode = get_node("/root/World/HUD")
	self.connect("got_hit", CanvasNode, "increase_score", [1])
	self.connect("got_destroyed", CanvasNode, "increase_score", [50])
	motion.x = -100
	set_ship_color(Color(1, 0.7, 0.3))
	pass

func _physics_process(delta):
	check_for_asteroids(delta)
	check_for_player()
	if get_global_transform().origin.x < -100:
		destroy()
	
	var collision = move_and_collide(motion * delta)
	
	if $DeadTimer.get_time_left() == 0 and not isAlive:
		get_parent().remove_child(self)

	if collision:
		if collision.collider.has_method("get_hit_points"):
			var src = null
			if collision.collider.has_method("get_source"):
				src = collision.collider.get_source()
			hit(collision.collider.get_hit_points(), src)
		if collision.collider.has_method("hit"):
			collision.collider.hit(get_hit_points(), self)
		update_alive_status()

func _draw():
	var vector = Vector2(700, 0)
	vector = vector.rotated(rotation)
	
#	draw_line(Vector2(0, 0), Vector2(700, 0), Color(1,1,1))
#	draw_line(startPosRight, castTargetRight, Color(1,1,1))
	pass

func check_for_player():
	var globalPosition = get_global_transform().origin
	var space_state = get_world_2d().direct_space_state

	var vector = Vector2(700, 0)
	vector = vector.rotated(rotation)

	var castTargetLeft = Vector2(globalPosition.x - 700, globalPosition.y + 4)
	var castTargetRight = Vector2(globalPosition.x - 700, globalPosition.y - 4)
	var startPosLeft = Vector2(globalPosition.x, globalPosition.y + 4)
	var startPosRight = Vector2(globalPosition.x, globalPosition.y - 4)
	var resultLeft = space_state.intersect_ray(startPosLeft, startPosLeft + vector, [self], 1)
	var resultRight = space_state.intersect_ray(startPosRight, startPosRight + vector, [self], 1)
	var leftSee = false
	var rightSee = false
	var canShoot = true
	if resultLeft:
		if resultLeft.collider.name == "Player":
			leftSee = true
		else:
			canShoot = false
	if resultRight:
		if resultRight.collider.name == "Player":
			rightSee = true
		else:
			canShoot = false

	if (leftSee or rightSee) and canShoot:
		shoot()

func check_for_asteroids(delta):
	var globalPosition = get_global_transform().origin
	var space_state = get_world_2d().direct_space_state
	var castTargetLeft = Vector2(globalPosition.x - 200, globalPosition.y + rayOffset)
	var castTargetRight = Vector2(globalPosition.x - 200, globalPosition.y - rayOffset)
	var startPosLeft = Vector2(globalPosition.x, globalPosition.y + rayOffset)
	var startPosRight = Vector2(globalPosition.x, globalPosition.y - rayOffset)
	var resultLeft = space_state.intersect_ray(startPosLeft, castTargetLeft, [self], raycastCollisionMask)
	var resultRight = space_state.intersect_ray(startPosRight, castTargetRight, [self], raycastCollisionMask)
	
	if resultLeft and resultRight:
		if resultLeft.position.x > resultRight.position.x:
			go_up(delta, true)
		else:
			go_down(delta, true)
	elif resultLeft:
		go_up(delta, true)
	elif resultRight:
		go_down(delta, true)
	else:
		go_nowhere(delta, true)
	

func destroy():
	.destroy()
	$DeadTimer.start()

func hit(points, source):
	.hit(points, source)
	emit_signal("got_hit")
	if source == null:
		return
	if source.name == "Player" and not isAlive:
		emit_signal("got_destroyed")
