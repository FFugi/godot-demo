extends "res://scripts/Ship.gd"

var isWaiting = false

signal got_destroyed
signal rised_from_dead
signal health_changed

const MAX_HORIZONTAL_SPEED = 300

func _ready():
	var HUDNode = get_node("/root/World/HUD")
	self.connect("got_destroyed", HUDNode, "player_died")
	self.connect("rised_from_dead", HUDNode, "respawn")
	self.connect("health_changed", HUDNode, "change_health_bar", [self])
	emit_signal("health_changed")
	set_ship_color(Color(0.4, 0.86, 1))

func _physics_process(delta):
	if not isAlive and not isWaiting:
		$ZombieTimer.start()
		isWaiting = true
	
	if isWaiting and $ZombieTimer.get_time_left() == 0:
		make_alive()
	if isWaiting and $ZombieTimer.get_time_left() < 1 and $ZombieTimer.get_time_left() > 0.8:
		position = Vector2(100, 304)
	
	var rot = get_transform().get_rotation()
	if get_global_transform().origin.y < 0:
		destroy()
	elif get_global_transform().origin.y > 600:
		destroy()
	if Input.is_action_pressed("ui_up"):
		go_up(delta, false)
	elif Input.is_action_pressed("ui_down"):
		go_down(delta, false)
	else:
		go_nowhere(delta, false)
		
	if Input.is_action_pressed("ui_left"):
		go_backward(delta)
	elif Input.is_action_pressed("ui_right"):
		go_forward(delta)
	else:
		reduce_horizontal_speed(delta)
			
	if isAlive:
		if Input.is_action_pressed("ui_accept"):
			shoot()
	
	var collision = move_and_collide(motion * delta)

	if collision:
		if collision.collider.has_method("get_hit_points"):
			hit(collision.collider.get_hit_points(), self)
			print(String(collision.collider.get_hit_points()))
		if collision.collider.has_method("hit"):
			var src = null
			if collision.collider.has_method("get_source"):
				src = collision.collider.get_source()
			collision.collider.hit(hitPoints, src)
			
func go_forward(delta):
	if get_global_transform().origin.x < 400:
		motion.x = min(motion.x + 200 * delta, MAX_HORIZONTAL_SPEED)
	else:
		reduce_horizontal_speed(delta)
	
func go_backward(delta):
	if get_global_transform().origin.x > 100:
		motion.x = max(motion.x - 200 * delta, -MAX_HORIZONTAL_SPEED)
	else:
		reduce_horizontal_speed(delta)

func reduce_horizontal_speed(delta):
	if motion.x > 2:
		motion.x -= 500 * delta
	if motion.x < -2:
		motion.x += 500 * delta
		
func destroy():
	.destroy()
	emit_signal("got_destroyed")
	$Sprite.visible = true
	set_ship_color(Color(0.4, 0.86, 1, 0.3))
	
func hit(points, source):
	.hit(points, source)
	emit_signal("health_changed")

func make_alive():
	.make_alive()
	isWaiting = false
	emit_signal("health_changed")
	emit_signal("rised_from_dead")
	set_ship_color(Color(0.4, 0.86, 1))

func increase_health(value):
	.increase_health(value)
	emit_signal("health_changed")